# Read Me - Macho

Originally seen from HiPhish at https://hiphish.github.io/
Further expansion from DT Distrotube at https://www.youtube.com/distrotube

## Dependencies
- man
- apropos
- fzf
- dmenu/rofi (if you're using the gui)
- awk
- sed
- grep
- zathura (if you're using the gui)
- groff (if you're using the gui, and not groff-base)

